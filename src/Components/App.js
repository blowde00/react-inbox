import MessageList from './MessageList';

function App() {


  return (
    <div className="App">
      <MessageList />
    </div>
  );
}

export default App;
