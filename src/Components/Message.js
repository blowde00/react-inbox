import React from "react";

const Message = ({ message, checkbox, handleStar }) => {

  return (
    <form>
      <div className={`row message ${message.read ? 'read' : 'unread'} ${message.selected ? 'selected' : ''}`}>
        <div className="col-xs-1">
          <div className="row">
            <div className="col-xs-2">
              <input type="checkbox" checked={message.selected ? 'checked' : ''} onChange={() => checkbox(message.id)} />
            </div>
            <div className="col-xs-2">
              <i className={`star fa ${message.starred ? 'fa-star' : 'fa-star-o'}`} onClick={() => handleStar(message.id)}></i>
            </div>
          </div>
        </div>
        <div className="col-xs-11">
          <span className="label label-warning">{message.labels.includes('dev') ? 'dev' : ''}</span>
          <span className="label label-warning">{message.labels.includes('personal') ? 'personal' : ''}</span>
          <span className="label label-warning">{message.labels.includes('gschool') ? 'gschool' : ''}</span>
          <a href="#">
            {message.subject}
          </a>
        </div>
      </div>
    </form>
  )
}

export default Message;