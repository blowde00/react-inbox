import React, { useContext } from "react";
import MessageContext from "../Context/MessageContext";

const Toolbar = ({ messages }) => {
  const [dispatch] = useContext(MessageContext);

  function trashcan() {
    const trashList = messages.filter(message => { return message.selected === false })
    dispatch({ type: 'delete', payload: trashList })
  }

  function makeRead() {
    const readList = messages.map((message) => {
      if (message.selected === true) {
        message.read = true
        return message;
      }
      return message;
    })
    dispatch({ type: 'read', payload: readList })
  }

  function makeUnread() {
    const unreadList = messages.map((message) => {
      if (message.selected === true) {
        message.read = false
        return message;
      }
      return message;
    })
    dispatch({ type: 'unread', payload: unreadList })
  }

  function unreadNumber() {
    const unreadMessages = messages.filter(message => { return message.read === false })
    return unreadMessages.length;
  }

  function selectAllIcon() {
    let checkIcon = '';
    const checkedMessages = messages.filter(message => { return message.selected === true })
    if (checkedMessages.length === 0) {
      checkIcon = 'fa-square-o'
    } else if (checkedMessages.length === messages.length) {
      checkIcon = 'fa-check-square-o'
    } else {
      checkIcon = 'fa-minus-square-o'
    }
    return checkIcon;
  }

  function handleSelectAll() {
    const checkedMessages = messages.filter(message => { return message.selected === true })
    if (checkedMessages.length === messages.length) {
      let selectNone = messages.map((message) => {
        message.selected = false;
        return message;
      })
      dispatch({ type: 'selectAll', payload: selectNone })

    } else {
      let selectAll = messages.map((message) => {
        message.selected = true;
        return message;
      })
      dispatch({ type: 'selectAll', payload: selectAll })
    }
  }

  function applyLabel(e) {
    if (e.target.value === 'dev') {
      const selectedList = messages.map((message) => {
        if (message.selected === true) {
          if (message.labels.includes('dev')) {
            return message;
          } else {
            message.labels.push('dev')
            return message;
          }
        }
        return message;
      })
      dispatch({ type: 'applyLabel', payload: selectedList })
    }

    if (e.target.value === 'personal') {
      const selectedList = messages.map((message) => {
        if (message.selected === true) {
          if (message.labels.includes('personal')) {
            return message;
          } else {
            message.labels.push('personal')
            return message;
          }
        }
        return message;
      })
      dispatch({ type: 'applyLabel', payload: selectedList })
    }

    if (e.target.value === 'gschool') {
      const selectedList = messages.map((message) => {
        if (message.selected === true) {
          if (message.labels.includes('gschool')) {
            return message;
          } else {
            message.labels.push('gschool')
            return message;
          }
        }
        return message;
      })
      dispatch({ type: 'applyLabel', payload: selectedList })
    }
  }

  function removeLabel(e) {
    if (e.target.value === 'dev') {
      const selectedList = messages.map((message) => {
        if (message.selected === true) {
          message.labels = message.labels.filter(label => label !== 'dev')
          return message;
        }
        return message;
      })
      dispatch({ type: 'removeLabel', payload: selectedList })
    }

    if (e.target.value === 'personal') {
      const selectedList = messages.map((message) => {
        if (message.selected === true) {
          message.labels = message.labels.filter(label => label !== 'personal')
          return message;
        }
        return message;
      })
      dispatch({ type: 'removeLabel', payload: selectedList })
    }

    if (e.target.value === 'gschool') {
      const selectedList = messages.map((message) => {
        if (message.selected === true) {
          message.labels = message.labels.filter(label => label !== 'gschool')
          return message;
        }
        return message;
      })
      dispatch({ type: 'removeLabel', payload: selectedList })
    }
  }

  function handleToolbar() {
    const selectedMessages = messages.filter(message => { return message.selected === true })
    return selectedMessages.length;
  }

  return (
    <div className="row toolbar">
      <div className="col-md-12">
        <p className="pull-right">
          <span className="badge badge">{unreadNumber()}</span>
          {unreadNumber() !== 1 ? 'unread messages' : 'unread message'}
        </p>

        <button className="btn btn-default" onClick={() => handleSelectAll()}>
          <i className={`fa ${selectAllIcon()}`}></i>
        </button>

        <button className="btn btn-default" disabled={handleToolbar() > 0 ? '' : 'disabled'} onClick={() => makeRead()}>
          Mark As Read
        </button>

        <button className="btn btn-default" disabled={handleToolbar() > 0 ? '' : 'disabled'} onClick={() => makeUnread()}>
          Mark As Unread
        </button>

        <select className="form-control label-select" disabled={handleToolbar() > 0 ? '' : 'disabled'} onChange={(e) => applyLabel(e)}>
          <option>Apply label</option>
          <option value="dev">dev</option>
          <option value="personal">personal</option>
          <option value="gschool">gschool</option>
        </select>

        <select className="form-control label-select" disabled={handleToolbar() > 0 ? '' : 'disabled'} onChange={(e) => removeLabel(e)}>
          <option>Remove label</option>
          <option value="dev">dev</option>
          <option value="personal">personal</option>
          <option value="gschool">gschool</option>
        </select>

        <button className="btn btn-default" disabled={handleToolbar() > 0 ? '' : 'disabled'} onClick={() => trashcan()}>
          <i className="fa fa-trash-o"></i>
        </button>
      </div>
    </div>
  )
}

export default Toolbar;