import React, { useReducer } from "react";
import Message from "./Message";
import { seedData } from "./SeedData";
import MessageContext from "../Context/MessageContext";
import Toolbar from "./Toolbar";

const initialState = {
  messages: seedData
}

function reducer(state, action) {
  switch (action.type) {
    case 'checked':
    case 'delete':
    case 'read':
    case 'unread':
    case 'star':
    case 'selectAll':
    case 'applyLabel':
    case 'removeLabel':
      return { ...state, messages: action.payload };
    default:
      return state;
  }
}

const MessageList = () => {
  const [state, dispatch] = useReducer(reducer, initialState)

  function checkbox(id) {
    let messageInput = state.messages.map((message) => {
      if (message.id === id) {
        message.selected = !message.selected
        return message;
      }
      return message;
    })
    dispatch({ type: 'checked', payload: messageInput })
  }

  function handleStar(id) {
    let messageInput = state.messages.map((message) => {
      if (message.id === id) {
        message.starred = !message.starred
        return message;
      }
      return message;
    })
    dispatch({ type: 'star', payload: messageInput })
  }

  return (
    <MessageContext.Provider value={[dispatch]}>
      <>
        <Toolbar messages={state.messages} />
      </>
      <ul>{state.messages.map(message => {
        return <Message key={message.id} message={message} checkbox={checkbox} handleStar={handleStar} />
      })}
      </ul>
    </MessageContext.Provider>
  )
}

export default MessageList;